# load libraries
library(readxl)
library(r2d3)
library(DataExplorer)
library(dplyr)
library(magrittr)
library(ggplot2)
library(ggthemes)
library(janitor)
library(caret)
library(hydroGOF)
library(randomForest)
library(padr)
library(e1071)

# read data
df <-
  read_excel("Data collection sheet for Tulcea CQ174 trial_August19.xlsx") %>%
  janitor::clean_names()

df <- df %>%
  filter(x45_mm_of_of_last_tank_a > 0.001)

# train / test sets

dfTRAIN <- df %>%
  filter(date < '2019-06-15')
dfTEST <- df %>%
  filter(date >= '2019-06-15')

## build X dataframe

Xtrain <- dfTRAIN %>%
  select(
    "date",
    "total_ltp_flow_m3_h",
    "na2ok_of_ltp_g_l",
    "a_c_of_ltp",
    "fine_seed_charge_of_line_a_g_l",
    "solid_content_of_1st_growth_tank_of_line_a_g_l",
    "fine_seed_charge_of_line_b_g_l",
    "solid_content_of_1st_growth_tank_of_line_b_g_l",
    "percent_45_of_fine_seed",
    "percent_20_of_fine_seed",
    "percent_10_of_fine_seed",
    "percent_10_of_coarse_seed",
    "percent_20_of_coarse_seed",
    "percent_45_of_coarse_seed",
    "cgm_number",
    "cgm_dose_ppm",
    "x1st_tank_of_line_a_temp",
    "last_tank_of_line_a_temp",
    "x1st_tank_of_line_b_temp",
    "last_tank_of_line_b_temp"
  ) %>%
  data.frame()

Xtrain <- Xtrain %>%
  select(
    -"solid_content_of_1st_growth_tank_of_line_a_g_l",
    -"solid_content_of_1st_growth_tank_of_line_b_g_l",
    -"cgm_number",
    -"date",
    -"percent_45_of_coarse_seed",
    -"percent_20_of_coarse_seed",
    -"percent_10_of_coarse_seed",
    -"fine_seed_charge_of_line_b_g_l"
  )

plot_missing(Xtrain)

Xtest <- dfTEST %>%
  select(
    "date",
    "total_ltp_flow_m3_h",
    "na2ok_of_ltp_g_l",
    "a_c_of_ltp",
    "fine_seed_charge_of_line_a_g_l",
    "solid_content_of_1st_growth_tank_of_line_a_g_l",
    "fine_seed_charge_of_line_b_g_l",
    "solid_content_of_1st_growth_tank_of_line_b_g_l",
    "percent_45_of_fine_seed",
    "percent_20_of_fine_seed",
    "percent_10_of_fine_seed",
    "percent_10_of_coarse_seed",
    "percent_20_of_coarse_seed",
    "percent_45_of_coarse_seed",
    "cgm_number",
    "cgm_dose_ppm",
    "x1st_tank_of_line_a_temp",
    "last_tank_of_line_a_temp",
    "x1st_tank_of_line_b_temp",
    "last_tank_of_line_b_temp"
  ) %>%
  data.frame()

Xtest <- Xtest %>%
  select(
    -"solid_content_of_1st_growth_tank_of_line_a_g_l",
    -"solid_content_of_1st_growth_tank_of_line_b_g_l",
    -"cgm_number",
    -"date",
    -"percent_45_of_coarse_seed",
    -"percent_20_of_coarse_seed",
    -"percent_10_of_coarse_seed",
    -"fine_seed_charge_of_line_b_g_l"
  )

plot_missing(Xtest)

## plots

## model the y's

### x45_mm_of_of_last_tank_a

ytrain <- dfTRAIN %>%
  select("x45_mm_of_of_last_tank_a") %>%
  data.frame()

ytest <- dfTEST %>%
  select("x45_mm_of_of_last_tank_a") %>%
  data.frame()

Train <- cbind(Xtrain, ytrain)
Test  <- cbind(Xtest, ytest)

#### random forest

rf <- randomForest(x45_mm_of_of_last_tank_a ~ ., data = Train, importance = TRUE, na.action = remove())

X_pred <-
  predict(rf, Train %>% select(-x45_mm_of_of_last_tank_a,)) %>%
  data.frame()

TRAIN <- cbind(Train$x45_mm_of_of_last_tank_a, X_pred)
colnames(TRAIN) <- c('meas', 'pred')

TRAIN$set <- 'train'

X_pred <-
  predict(rf, Test %>% select(-x45_mm_of_of_last_tank_a)) %>%
  data.frame()

TEST <- cbind(Test$x45_mm_of_of_last_tank_a, X_pred)
colnames(TEST) <- c('meas', 'pred')

TEST$set <- 'test'

result <- rbind(TRAIN, TEST)

result02 <- result
result02$date <- df$date

result02 <- pad(result02, interval = "day")

ggplot(result02, aes(x = date, y = meas)) +
  geom_line(color = '#0000ff', size = 1.15) +
  geom_line(aes(x = date, y = pred), color = '#ffa500', size = 1.15) +
  ggthemes::theme_clean() +
  ylab('< 45mm of last tank -A: measured (blue) & predicted (orange)') +
  geom_vline(
    xintercept = as.POSIXct('2019-06-15'),
    linetype = "dashed",
    color = "gray",
    size = 1.15
  ) +
  geom_text(x = as.POSIXct('2019-05-01'),
            y = 40,
            label = "Measured",
            color = 'blue') +
  geom_text(x = as.POSIXct('2019-05-01'),
            y = 38,
            label = "Predicted",
            color = 'orange') +
  geom_text(x = as.POSIXct('2019-06-03'),
            y = 45,
            label = "June 15",
            color = 'gray')


